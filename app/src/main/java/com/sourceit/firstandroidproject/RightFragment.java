package com.sourceit.firstandroidproject;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class RightFragment extends Fragment implements  FragmentCommunication {

    public static final String INFO = "info";

    public static RightFragment newInstance() {
        RightFragment fragment = new RightFragment();
        Bundle args = new Bundle();
      //  args.putString(INFO, info);
        fragment.setArguments(args);
        return fragment;
    }

    String info;
    @BindView(R.id.info)
    TextView infoView;
    Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            info = getArguments().getString(INFO);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_info, container, false);
        unbinder = ButterKnife.bind(this, root);
        infoView.setText(info);
        return root;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void updateText(String str) {

        infoView.setText(str);
    }
}
