package com.sourceit.firstandroidproject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;

public class LeftFragment extends Fragment {

    String name;
    @BindView(R.id.some_text)
    TextView label;
    @BindView(R.id.input_text_name)
    EditText inputTextName;
    @BindView(R.id.input_text_age)
    EditText inputTextAge;
    Unbinder unbinder;
    FragmentCommunication fragmentCommunication;

    public static LeftFragment newInstance() {
        LeftFragment fragment = new LeftFragment();
        Bundle args = new Bundle();
    //    args.putString("name", name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            name = getArguments().getString("name");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_blank, container, false);
        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @OnClick(R.id.action)
    public void onPresentClick() {

        fragmentCommunication.updateText(label.getText().toString());
       /* getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container,
                        RightFragment.newInstance(label.getText().toString()))
                .addToBackStack(null)
                .commit();
                */


    }

    @OnTextChanged({R.id.input_text_age, R.id.input_text_name})
    public void onTextChange(Editable s) {
        label.setText(inputTextName.getText().toString() + ", " +
                inputTextAge.getText().toString());
    }

    public void setFragmentCommunication(FragmentCommunication fragmentCommunication) {
        this.fragmentCommunication = fragmentCommunication;
    }
}
