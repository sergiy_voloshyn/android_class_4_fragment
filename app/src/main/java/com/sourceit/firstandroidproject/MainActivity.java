package com.sourceit.firstandroidproject;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LeftFragment leftFragment =LeftFragment.newInstance();
        RightFragment rightFragment=RightFragment.newInstance();
        leftFragment.setFragmentCommunication(rightFragment);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_left, leftFragment)
                .replace(R.id.container_right, rightFragment)
                .commit();



    }

}
